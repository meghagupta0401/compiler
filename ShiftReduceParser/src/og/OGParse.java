package og;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;
class Production{
	String LHS;
	String RHS;
}
class Precedence{
	String LHS;
	String RHS;
	int value;//-1 for .> //0 for .= //1 for <. //9 for not possible/invalid
}
class State{
	String stack;
	String sentence;
}
public class OGParse {
	static ArrayList<Production> Grammar;
	static ArrayList<Precedence> PrecedenceTable;
	public static void main(String[] args) {
		Grammar=getGrammar("operatorgrammar.txt");
		PrecedenceTable=getPrecedence("precedencetable.txt");
		printPrecdenceTable(PrecedenceTable);
		System.out.println("Enter String to parse:");
		Scanner in=new Scanner(System.in);
		String input=in.nextLine();
		parse(input);
	}
	public static ArrayList<Production> getGrammar(String filename){
		ArrayList<Production> Grammar=new ArrayList<Production>();
		String grammar=	File.readFile(filename);

		String[] rule = grammar.split(",");
		ArrayList<String> rules=new ArrayList<String>();
		for(String current:rule){
			rules.add(current);
		}
		Iterator<String> rule_iterator=rules.iterator();
		while(rule_iterator.hasNext()){
			String grammarset=rule_iterator.next();
			String LHS=grammarset.substring(0, grammarset.indexOf("-"));
			Production production;
			StringTokenizer RHSset=new StringTokenizer(grammarset.substring(grammarset.indexOf(">")+1,grammarset.length()),"|");

			while(RHSset.hasMoreTokens()){
				String RHS = RHSset.nextToken();
				production=new Production();
				production.LHS=LHS;
				production.RHS=RHS;
				Grammar.add(production);
			}
		}
		return Grammar;
	}
	public static ArrayList<Precedence> getPrecedence(String filename){
		ArrayList<Precedence> PrecedenceTable=new ArrayList<Precedence>();
		String table=	File.readFile(filename);
		String top_headers=table.substring(0,table.indexOf(","));
		String [] heads=top_headers.split("\\|");
		for(int i=1;i<heads.length;i++){
			heads[i]=heads[i].trim();
		}
		String table_body=table.substring(table.indexOf(",")+1);
		String[] rows = table_body.split(",");

		for(int j=0;j<rows.length;j++){
			String[] row=rows[j].split("\\|");
			for(int k=1;k<row.length;k++){
				Precedence current= new Precedence();
				current.LHS=row[0].trim();
				current.RHS=heads[k].trim();
				//-1 for .> //0 for .= //1 for <. //9 for not possible/invalid
				if(row[k].equals(".>"))current.value=-1;
				else if(row[k].equals(".="))current.value=0;
				else if(row[k].equals("<."))current.value=1;
				else current.value=9;
				PrecedenceTable.add(current);
			}
		}


		return PrecedenceTable;
	}
	public static void printProductions(){
		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){
			Production current = iterator.next();
			System.out.println(current.LHS+"->"+current.RHS);
		}
	}
	public static int compare(String lhs,String rhs){
 //  System.out.println("comparing "+lhs+" with "+rhs);
		Iterator<Precedence> iterator = PrecedenceTable.iterator();
		while(iterator.hasNext()){
			Precedence precednce=iterator.next();
			if(precednce.LHS.equals(lhs)&&precednce.RHS.equals(rhs))
				return precednce.value;
		}
		System.out.println("Error");
		System.exit(0);
		return 9;
	}
	public static void printPrecdenceTable(ArrayList<Precedence> precedence_table){
		Iterator<Precedence> iterator = precedence_table.iterator();
		while(iterator.hasNext()){
			Precedence current = iterator.next();
			String comp;
			if(current.value==-1)comp=".>";
			else if(current.value==0)comp=".=";
			else if(current.value==1)comp="<.";
			else comp="XX";

			System.out.println(current.LHS+comp+current.RHS);
		}
	}
	public static void parse(String sentence){
		State state=new State();
		state.stack="$";
		state.sentence=sentence;
		Read(state);
	}
	public static void Read(State state){
		System.out.println("Action     Stack      Sentence    Production used");
		State current=new State();
		current.stack=state.stack;
		current.sentence=state.sentence;
		while(!current.stack.equals("$E")|| !current.sentence.equals("")){

			if(current.sentence.equals("")&&current.stack.equals("")){System.out.println("Failed1");return ;}
			int action_code = -1;
		//	System.out.print("currentstack->"+current.stack+"      ");
		//	System.out.print("currentsentence->"+current.sentence+"      ");
			if(current.sentence.length()!=0)
			{
			if(current.stack.substring(current.stack.length()-1,current.stack.length()).equals("E"))
			action_code=compare(current.stack.substring(current.stack.length()-2,current.stack.length()-1), current.sentence.substring(0,1));
			else
				action_code=compare(current.stack.substring(current.stack.length()-1,current.stack.length()), current.sentence.substring(0,1));	
			if(!current.sentence.equals("")&&(action_code==0||action_code==1)){
				System.out.print("\nShift      ");
				current.stack+=current.sentence.substring(0,1);//append
				current.sentence=current.sentence.substring(1,current.sentence.length());//remove
				System.out.print(current.stack+"        ");
				System.out.print(current.sentence+"        ");
			}
			
		}
			
			Iterator<Production> iterator = Grammar.iterator();
			String rhs="",lhs="",temp_rhs="";
			while(iterator.hasNext()){
				Production rule = iterator.next();
				for(int i=0;i<current.stack.length();i++){
                   
					if(current.stack.substring(current.stack.length()-i-1,current.stack.length()).equals(rule.RHS))
					{
						temp_rhs=current.stack.substring(current.stack.length()-i-1,current.stack.length());
						if(rhs.length()<=temp_rhs.length()){
							//System.out.println("rulematch"+action_code);
							rhs=temp_rhs;
							lhs=rule.LHS;}
					}
				}
			}
			if(!rhs.equals("")&&action_code==-1){
				
				StringBuilder b = new StringBuilder(current.stack);
				b.replace(current.stack.lastIndexOf(rhs),current.stack.lastIndexOf(rhs)+rhs.length()+1,lhs);
				current.stack=b.toString();
				System.out.print("    "+lhs+"->"+rhs);
				System.out.print("\nReduce      ");
				System.out.print(current.stack+"      ");
				System.out.print(current.sentence+"     ");
				 
				
			}
			 
			if(rhs.equals("")&&current.sentence.equals("")){System.out.println("Failed");return ;}
			if(current.sentence.equals("")&& current.stack.equals("$E")){System.out.println("Success");return ;}


		}
	}
}
