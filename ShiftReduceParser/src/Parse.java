import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;
class Production{
	String LHS;
	String RHS;
}
class State{
	String stack;
	String sentence;
}
public class Parse {
	static ArrayList<Production> Grammar;
	public static void main(String[] args) {
		Grammar=getGrammar("Grammar.txt");
		//printProductions(Grammar);
		System.out.println("Enter String to parse:");
		Scanner in=new Scanner(System.in);
		String input=in.nextLine();
		parse(input);
	}
	public static ArrayList<Production> getGrammar(String filename){
		ArrayList<Production> Grammar=new ArrayList<Production>();
		String grammar=	File.readFile(filename);

		String[] rule = grammar.split(",");
		ArrayList<String> rules=new ArrayList<String>();
		for(String current:rule){
			rules.add(current);
		}
		Iterator<String> rule_iterator=rules.iterator();
		while(rule_iterator.hasNext()){
			String grammarset=rule_iterator.next();
			String LHS=grammarset.substring(0, grammarset.indexOf("-"));
			Production production;
			//	System.out.println(grammarset.substring(grammarset.indexOf(">")+1,grammarset.length()));
			StringTokenizer RHSset=new StringTokenizer(grammarset.substring(grammarset.indexOf(">")+1,grammarset.length()),"|");

			while(RHSset.hasMoreTokens()){
				String RHS = RHSset.nextToken();
				production=new Production();
				production.LHS=LHS;
				production.RHS=RHS;
				Grammar.add(production);
			}
		}
		return Grammar;
	}
	public static void printProductions(ArrayList<Production> productions){
		Iterator<Production> iterator = productions.iterator();
		while(iterator.hasNext()){
			Production current = iterator.next();
			System.out.println(current.LHS+"->"+current.RHS);
		}
	}
	public static void parse(String sentence){

		State state=new State();
		state.stack="";
		state.sentence=sentence;
		Read(state);
	}
	public static State Reduce(State state){

		if(state == null)return null;

		State current=new State();
		current.stack=state.stack;
		current.sentence=state.sentence;
		if(current.sentence.equals("")&&current.stack.equals("")){System.out.println("Failed");return null;}
		if(!current.sentence.equals("")){
			current.stack+=current.sentence.substring(0,1);//append
			current.sentence=current.sentence.substring(1,current.sentence.length());//remove
		}

		System.out.println("stack="+current.stack);
		System.out.println("sentence="+current.sentence);
		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){
			Production rule = iterator.next();
			for(int i=0;i<current.stack.length();i++){
				//	System.out.println(current.stack.substring(current.stack.length()-i-1,current.stack.length())+"---"+rule.RHS);
				String rhs="";
				if(current.stack.substring(current.stack.length()-i-1,current.stack.length()).equals(rule.RHS))
				{
					System.out.println("rulematched");
					rhs=current.stack.substring(current.stack.length()-i-1,current.stack.length());

					StringBuilder b = new StringBuilder(current.stack);
					b.replace(current.stack.lastIndexOf(rhs),current.stack.lastIndexOf(rhs)+rhs.length()+1,rule.LHS);

					State temp_state=new State();
					temp_state.stack=b.toString();
					System.out.println("changed "+rule.RHS+"to "+rule.LHS +" in "+current.stack);
					System.out.println("remaining stack "+b.toString());
					System.out.println("remaining sentence "+current.sentence);
					temp_state.sentence=current.sentence;
					if(temp_state.sentence.equals("")&& temp_state.stack.equals("S")){System.out.println("Success1");return null;}	
					Reduce(temp_state);
				}
				if(rhs.equals("")&&current.sentence.equals("")){System.out.println();return null;}


			}
		}

		if(current.sentence.equals("")&& current.stack.equals("S")){System.out.println("Success");return null;}

		else return Reduce(current);
	}
	public static void Read(State state){
		System.out.println("Action     Stack      Sentence    Production used");
		State current=new State();
		current.stack=state.stack;
		current.sentence=state.sentence;
		while(!current.stack.equals("S")&& !current.sentence.equals("")){

			if(current.sentence.equals("")&&current.stack.equals("")){System.out.println("Failed");return ;}

			if(!current.sentence.equals("")){
				System.out.print("\n Shift      ");
				current.stack+=current.sentence.substring(0,1);//append
				current.sentence=current.sentence.substring(1,current.sentence.length());//remove
			}

			System.out.print(current.stack+"      ");
			System.out.print(current.sentence+"      ");
			Iterator<Production> iterator = Grammar.iterator();
			String rhs="",lhs="",temp_rhs="",temp_lhs="";
			while(iterator.hasNext()){
				Production rule = iterator.next();
				for(int i=0;i<current.stack.length();i++){
					//	System.out.println(current.stack.substring(current.stack.length()-i-1,current.stack.length())+"---"+rule.RHS);

					if(current.stack.substring(current.stack.length()-i-1,current.stack.length()).equals(rule.RHS))
					{
						//	System.out.println("rulematched");
						temp_rhs=current.stack.substring(current.stack.length()-i-1,current.stack.length());
						//	System.out.println("rhs="+temp_rhs);
						if(rhs.length()<temp_rhs.length()){
							rhs=temp_rhs;
							lhs=rule.LHS;}
					}
				}
			}
			if(!rhs.equals("")){

				StringBuilder b = new StringBuilder(current.stack);
				b.replace(current.stack.lastIndexOf(rhs),current.stack.lastIndexOf(rhs)+rhs.length()+1,lhs);
				current.stack=b.toString();
				//State temp_state=new State();
				//temp_state.stack=b.toString();
				System.out.print("    "+lhs+"->"+rhs);
				System.out.print("\nReduce      ");
				System.out.print(current.stack+"      ");
				System.out.print(current.sentence+"     ");
				//System.out.println("changed "+rhs+"to "+lhs +" in "+current.stack);
				//System.out.println("remaining stack "+b.toString());
				//System.out.println("remaining sentence "+current.sentence);
				//temp_state.sentence=current.sentence;
			}
			//	if(temp_state.sentence.equals("")&& temp_state.stack.equals("S")){System.out.println("Success1");return ;}	

			if(rhs.equals("")&&current.sentence.equals("")){System.out.println("Failed");return ;}





			if(current.sentence.equals("")&& current.stack.equals("S")){System.out.println("Success");return ;}


		}
	}
}
