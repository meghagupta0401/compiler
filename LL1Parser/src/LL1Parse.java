import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;


class Production{
	String LHS;
	String RHS;
}

class First{
	String word;
	Boolean converged=false;
	ArrayList<String> first;
}

public class LL1Parse {


	static ArrayList<Production> Grammar;
	public static void main(String[] args) {
		Grammar=getGrammar("Grammar.txt");
		//printProductions();
		System.out.println("Enter String to find first:");
		Scanner in=new Scanner(System.in);
		String input=in.nextLine();
		ArrayList<String> first =getFirst(input);
		System.out.println("FIRST of "+input);
		Iterator<String>iterator=first.iterator();
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
	}
	public static ArrayList<Production> getGrammar(String filename){
 		ArrayList<Production> Grammar=new ArrayList<Production>();
		String grammar=	File.readFile(filename);

		String[] rule = grammar.split(",");
		ArrayList<String> rules=new ArrayList<String>();
		for(String current:rule){
			rules.add(current);
		}
		Iterator<String> rule_iterator=rules.iterator();
		while(rule_iterator.hasNext()){
			String grammarset=rule_iterator.next();
			String LHS=grammarset.substring(0, grammarset.indexOf("-"));
			Production production;
			StringTokenizer RHSset=new StringTokenizer(grammarset.substring(grammarset.indexOf(">")+1,grammarset.length()),"|");

			while(RHSset.hasMoreTokens()){
				String RHS = RHSset.nextToken();
				production=new Production();
				production.LHS=LHS;
				production.RHS=RHS;
				Grammar.add(production);
			}
		}
		return Grammar;
	}
	public static void printProductions(){
		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){
			Production current = iterator.next();
			System.out.println(current.LHS+"->"+current.RHS);
		}
	}
	
	public static ArrayList<String> getFirst(String word){
		ArrayList<String> first=new ArrayList<String>();
		
		if(word.length()==0||word.equals("^")){ if(!first.contains("^"))first.add("^");}
		else {      
			if(word.length()==1 && !isTerminal(word.charAt(0))){
					if(hasNull(word.substring(0,1)))
					first.add("^");
						}
			
			boolean completed=true;
					for(int i=0;i<=word.length();i++){
						
						if(i==word.length()){{ if(!first.contains("^"))first.add("^");}break;}	
						
						if(isTerminal(word.charAt(i))){
							
							if(!first.contains(word.substring(i,i+1)))first.add(word.substring(i,i+1));
							break;
						}
						else{
							//non terminal found
							ArrayList<String> rhs_list=getRHS(word.substring(i,i+1));
							Iterator<String>rhs_iterator=rhs_list.iterator();
							while(rhs_iterator.hasNext()){
								String rhs=rhs_iterator.next();
								if (rhs.charAt(0)=='^')completed=false;
								else if(isTerminal(rhs.charAt(0)))first.add(rhs.substring(0, 1));
								else {
									
									for(int j=0;j<rhs.length();j++)
									{ boolean done=true;	
									if(!word.substring(0, 1).equals(rhs.substring(j, j+1))) {
										ArrayList<String> recurse=getFirst(rhs.substring(j, j+1));//recursive step
									
										Iterator<String> recurse_iterator = recurse.iterator();
										while(recurse_iterator.hasNext()){
											String current=recurse_iterator.next();
											if(!current.equals("^")){
												if(!first.contains(current))first.add(current);  //add all except null
												done=false;
											}
											else completed=false;
											
											
										}
									}
										if(done)break;
										//if(j==rhs.length()-1&&i==word.length()-1)first.add("^");	
									}
									
								}
							}
						if(hasNull(word.substring(i,i+1)))completed=false;
							if(completed)break;
						}
						
						
					}
					
					
		}
		
		 		
return first;
	}
	static boolean isTerminal(char x){
		if (x=='^') return false;
		else if((int)x>=65 && (int)x<=91)return false;
		else return true;
	}
	static boolean hasNull(String LHS){
		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){
			Production current=iterator.next();
			if(LHS.equals(current.LHS)){
				if(current.RHS.equals("^"))
				return true;
			}
		}
		return false;
	}
	static ArrayList<String> getRHS(String LHS){
		ArrayList<String> list=new ArrayList<String>();
		
		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){
			Production current=iterator.next();
			if(LHS.equals(current.LHS)){
				list.add(current.RHS);
			}
		}
		return list;
	}
	
	}


