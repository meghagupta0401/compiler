package LexicalAnalyzer;

public enum TokenState {
SINGLEOP(1),DOUBLEOP(2);


	public final int state;
	private TokenState(int state){
		this.state=state;
	}
	public int getTokenState(){
		return state;
	}
}
