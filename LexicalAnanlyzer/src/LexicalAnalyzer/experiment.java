package LexicalAnalyzer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class experiment {
	String data;
	static ArrayList<Token>tokenlist;
	ArrayList<String>tokens;
	String operator_regex="";
	String keyword_regex="";
	static PrintWriter identifier=null;
	static PrintWriter constant=null;
	static PrintWriter keyword=null;
	static PrintWriter operator=null;
	public experiment(String data){
		
		
		try {
			identifier = new PrintWriter("identifier.txt", "UTF-8");
			constant = new PrintWriter("constant.txt", "UTF-8");
			keyword = new PrintWriter("keyword.txt", "UTF-8");
			operator = new PrintWriter("operator.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		this.data=data;
		tokens=new ArrayList<String>();
		tokenlist=new ArrayList<Token>();
		data=getStringConstants(data);

		ArrayList<String> OperatorList = getOperatorList();
		ArrayList<String> KeywordList=getKeywordsList();
		Iterator<String> operatorIterator = OperatorList.iterator();
		Iterator<String> keywordIterator = KeywordList.iterator();
		String current;
		while(operatorIterator.hasNext()){
			current=operatorIterator.next();
			operator_regex+=Pattern.quote(current)+"|";

		}
		while(keywordIterator.hasNext()){
			current=keywordIterator.next();
			//	keyword_regex+=Pattern.quote(current)+"|";	
			keyword_regex+=current+"|";		
		}
		//	System.out.println(keyword_regex);
		operator_regex=operator_regex.substring(0,operator_regex.length()-1);
		keyword_regex=keyword_regex.substring(0,keyword_regex.length()-1);


		//removing comments;
	String temp=data;
while(temp.contains("//")){
	temp=data;
	int start=temp.indexOf("//");
	int end=temp.indexOf("\n",start+1);
	//System.out.println(start+" "+end);
	data=temp.substring(0,start)+temp.substring(end,temp.length());
	temp=data;
}
//while(temp.contains("\\*")){
//	temp=data;
//	int start=temp.indexOf("\\");
//	int end=temp.indexOf("\n",start+1);
//	data=temp.substring(0,start)+temp.substring(end,temp.length());
//}


		data=data.replaceAll("\r", "").replaceAll("\n","").replaceAll("  ", " ");



		regexTokenize(data);
		//System.out.println("Remaining"+tokens.size());
		
	}

	public static ArrayList<String> getOperatorList(){
		String data=FileProcess.readFile("operators.csv");


		ArrayList<String> operators = new ArrayList<>(); 
		StringBuffer operator= new StringBuffer();
		char[] dataArray = data.toCharArray();
		for(int i=0;i<data.length();i++){
			if(dataArray[i]!=','){
				operator.append(dataArray[i]);
			}
			else{
				operators.add(operator.toString());
				//	System.out.println("|"+operator.toString()+"|");
				operator.delete(0, operator.length());

			}
		}
		operators.add(",");	
		return operators;

	}
	public static ArrayList<String> getKeywordsList(){
		String data=FileProcess.readFile("keywords.csv");


		ArrayList<String> operators = new ArrayList<>(); 
		StringBuffer operator= new StringBuffer();
		char[] dataArray = data.toCharArray();
		for(int i=0;i<data.length();i++){
			if(dataArray[i]!=','){
				operator.append(dataArray[i]);
			}
			else{
				operators.add(operator.toString());
				//	System.out.println("|"+operator.toString()+"|");
				operator.delete(0, operator.length());

			}
		}

		return operators;

	}
	public void  regexTokenize(String data){
		int i=0;
		int j=0;
		//	System.out.println(data);
		while(i>=0){
			j=data.indexOf(" ",i+1);
			//	System.out.println(i+"to"+j);
			if(i<0||j<=0)break;

			String token=data.substring(i+1,j)+" ";
			String remains=   detectRegex(token,keyword_regex,"keyword");
			tokens.remove(token);

			if(!remains.equals(" ")&&remains.length()!=0)
			{

				remains=  detectRegex(remains,operator_regex,"operator");
				remains=remains.replaceAll(" ", "");
				if(remains.length()!=0){
					Pattern r1 = Pattern.compile(("([a-zA-Zd])"));
					String remanant = token;
					Matcher m1 = r1.matcher(remains);
					//	System.out.println(remains);

					if (m1.find())
					{
						//		if(m1.group(0)!=null)System.out.println(remains+" is an identifier");
						tokenlist.add(new Token("identifier",remains));
						tokens.remove(remains);
					}
					else{
						Pattern r11 = Pattern.compile(("(\\d)"));
						String remanant1 = token;
						Matcher m11 = r11.matcher(remains);
						tokenlist.add(new Token("constant",remains));

						if (m11.find())
						{
							//	if(m11.group(0)!=null)System.out.println(remains+" is a constant");

							tokens.remove(remains);
						}
						else
						{
							// 			System.out.println("syntax error");
						}
					}
				}

				if(remains.length()==0)tokens.remove(remains);
				//   System.out.println("|"+remains+"| in |" +token+"| from "+ i+"to"+j); 
			}
			i=data.indexOf(" ",j);
		}
	}



	public String getStringConstants(String data){

		String code="";
		int start=0,end=0;
		Token current;
		while(data.indexOf("\"")>0){
			start=0;
			end=0;
			start=data.indexOf("\"");

			end=data.indexOf("\"", start+1)+1;

			current=new Token("constant",data.substring(start, end));


			tokenlist.add(current);
			//System.out.println("|||"+data.substring(start, end)+"|||");
			code= data.substring(0, start)+data.substring(end, data.length());

			data=code;
		}
		while(data.indexOf("\'")>0){
			start=0;
			end=0;
			start=data.indexOf("\'");

			end=data.indexOf("\'", start+1)+1;

			current=new Token("constant",data.substring(start, end));


			tokenlist.add(current);
			//System.out.println("|||"+data.substring(start, end)+"|||");
			code= data.substring(0, start)+data.substring(end, data.length());

			data=code;
		}
		while(data.indexOf("[")>0){
			start=0;
			end=0;
			start=data.indexOf("[");

			end=data.indexOf("]", start+1)+1;

			current=new Token("constant",data.substring(start, end));


			tokenlist.add(current);
			//System.out.println("|||"+data.substring(start, end)+"|||");
			code= data.substring(0, start)+data.substring(end, data.length());

			data=code;
		}
		

		return code;
	}
	public String detectRegex(String token,String regex,String type){

		Pattern r1 = Pattern.compile(regex);
		String remanant = token;
		Matcher m1 ;

		int match_size=0;
		Token current1=new Token(type,"");
		String match = null;
		String best_match=null;
		boolean done=false;
		while(!done){
			match_size=0;
			m1 = r1.matcher(remanant);
			while (m1.find())
			{
				match=m1.group();

				//	System.out.println("matching |"+remanant1+"| with|"+match+"|");
				if(match.length()>match_size){best_match=match;current1.setValue(match);match_size=match.length();

				}
			}	if(match_size>0){
				tokenlist.add(current1);
				//System.out.println("best matched|"+best_match+"|");
				remanant=remanant.replace(best_match, " ");

			}
			if(match_size==0) done=true;
		}
		return remanant;
	}



	public static void main(String[]args){
		new experiment(FileProcess.readFile("mag.c"));
		Iterator<Token> iterator = tokenlist.iterator();

		while(!tokenlist.isEmpty()){
			iterator = tokenlist.iterator();
			Token current = null;
		do{ if(iterator.hasNext())		current=iterator.next();
		else save();}
		while(current!=null&&current.isCounted());
			int count=1;
			current.setCounted(true);
			while(iterator.hasNext()){


				Token scan=iterator.next();
				if(scan.getType().equals(current.getType())&&scan.getValue().equals(current.getValue())&&! scan.isCounted()){
					count++;
					scan.setCounted(true);
					
				}
				
			}
			System.out.println(current.getType()+" "+current.getValue()+" "+count);
			
		
			if(current.getType().equals("identifier"))
			 identifier.println(current.getValue()+" "+count);
				
				
			if(current.getType().equals("constant"))
				constant.println(current.getValue()+" "+count);
				
				
			if(current.getType().equals("keyword"))
				keyword.println(current.getValue()+" "+count);
				
			if(current.getType().equals("operator"))
				operator.println(current.getValue()+" "+count);
				
			tokenlist.remove(current);
		
		}
	
	//getSingleOperatorList();
}
public static void save(){
	identifier.close();
	constant.close();
	keyword.close();
	operator.close();
	System.exit(0);
	}
}
